<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
    />
    <title>OpenSearch Talk 2023</title>

    <meta
      name="description"
      content="sett: data encryption and transfer made easy(ier)"
    />
    <meta name="author" content="Christian Ribeaud" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta
      name="apple-mobile-web-app-status-bar-style"
      content="black-translucent"
    />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>reveal.js</title>

    <link rel="stylesheet" href="dist/reset.css" />
    <link rel="stylesheet" href="dist/reveal.css" />
    <link rel="stylesheet" href="dist/theme/white.css" />
    <link rel="stylesheet" href="css/custom.css" />

    <link rel="stylesheet" href="plugin/highlight/monokai.css" />
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
        <section>
          <h2 style="margin-bottom: 100px">OpenSearch Ecosystem</h2>
          <p style="margin-bottom: 50px">by Christian Ribeaud</p>
          <p>
            <a
              href="https://gitlab.com/biomedit/presentations/opensearch-talk-2023"
              >gitlab.com/biomedit/presentations/opensearch-talk-2023</a
            >
          </p>
          <aside class="notes" data-markdown>
            <textarea data-template>
              - Who is using a monitoring/alerting system?
              - Who knows **OpenSearch**? **Elasticsearch**?
            </textarea>
          </aside>
        </section>
        <section>
          <h2>Outline</h2>
          <ul>
            <li>The story 🗞️</li>
            <li>Elastic Stack 🪆</li>
            <li>OpenSearch Ecosystem 🎍</li>
            <li>BioMedIt Infrastructure 🚧</li>
          </ul>
          <aside class="notes" data-markdown>
            <textarea data-template>
             - Some notes...
            </textarea>
          </aside>
        </section>
        <section>
          <h2>The story 🗞️</h2>
          <ul>
            <li>
              Beginning of 2021, Elastic Search and the Elastic Stack licenses
              changed to
              <em>Server Side Public License</em> (SSPL)
            </li>
            <li>
              AWS forks Elastic Search and creates its own version of the
              software, called <strong>OpenSearch</strong>.
            </li>
            <li>July 2021, <strong>OpenSearch 1.0</strong> is launched.</li>
          </ul>
          <aside class="notes" data-markdown>
            <textarea data-template>
              - The **SSPL**, a license which was introduced by MongoDB Inc., 
                forbids other companies than **Elastic** to offer **Elasticsearch** as a service (`SaaS`). 
              - If you just use **Elasticsearch** as the backend for your application - you are good to go. But there is a lot of gray area,
                such as embedding **Elasticsearch** as part of a larger solution that is sold as one piece, exposing some APIs that could
                be seen as **Elasticsearch APIs** (e.g. search via API), and so on. We wanted to have **zero risk**, especially because we
                don’t need anything special from **Elasticsearch**. This is why we opted to use **OpenSearch** and its basic features.
              - Actually, we even discussed pricing with **Elasticsearch**. They did not care that we are a non-profit organization and the fees
                was just incredibly high.
            </textarea>
          </aside>
        </section>
        <section>
          <section>
            <h2>Elastic Stack 🪆</h2>
            <div>
              <ul>
                <li class="fragment fade-up">
                  <strong>Elasticsearch</strong>: indexes, analyzes, and
                  searches the ingested data.
                </li>
                <li class="fragment fade-up">
                  <strong>Logstash</strong>: ingests, transforms, and sends
                  these data to the right destination.
                </li>
                <li class="fragment fade-up">
                  <strong>Kibana</strong>: visualizes the results of the
                  analysis.
                </li>
              </ul>
            </div>
            <div class="fragment">
              <img src="img/elk.png" />
            </div>
            <aside class="notes" data-markdown>
              <textarea data-template>
               - An equally popular, but less obvious use case is log analytics, in which you take the logs from an application,
                 feed them into **Elasticsearch**, and use the rich search and visualization functionality to identify issues.
              </textarea>
            </aside>
          </section>
          <section>
            <h2>The Beats Family 👨‍👩‍👧‍👦</h2>
            <div>
              <ul>
                <li class="fragment fade-in">
                  <strong>Filebeat</strong>: Lightweight shipper for logs and
                  other data
                </li>
                <li class="fragment fade-in">
                  <strong>Metricbeat</strong>: Lightweight shipper for metric
                  data
                </li>
                <li class="fragment fade-in">
                  <strong>Heartbeat</strong>: Lightweight shipper for uptime
                  monitoring
                </li>
              </ul>
            </div>
            <div class="fragment fade-in">
              <img src="img/elk-beats.jpg" width="500" />
            </div>
          </section>
        </section>
        <section>
          <section
            data-background-image="img/os-data-flow.png"
            data-background-size="80%"
            data-background-opacity="0.5"
            data-background-position="top"
          >
            <h2>OpenSearch Ecosystem 🎍</h2>
            <div>
              <ul>
                <li class="fragment">
                  <a href="https://fluentbit.io/"><strong>FluentBit</strong></a
                  >: popular <strong>Apache</strong>-liensed log forwarder. It
                  runs alongside an application, read log files, and forwards
                  them to a destination over HTTP
                </li>
                <li class="fragment">
                  <a href="https://opensearch.org/docs/latest/data-prepper/"
                    ><strong>Data Prepper</strong></a
                  >: receives the log lines and saves each of them as individual
                  <strong>OpenSearch</strong> document
                </li>
                <li class="fragment">
                  <strong>OpenSearch</strong>: distributed search and analytics
                  engine based on <strong>Apache Lucene</strong> (like
                  <strong>Elasticsearch</strong>)
                </li>
                <li class="fragment">
                  <strong>OpenSearch Dashboards</strong>: user interface that
                  lets you visualize your <strong>OpenSearch</strong> data
                </li>
              </ul>
            </div>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - **Data Prepper**: server-side data collector capable of filtering, enriching, transforming, normalizing,
                and aggregating data for downstream analytics and visualization.
              </textarea>
            </aside>
          </section>
        </section>
        <section>
          <section>
            <h2>BioMedIt Infrastructure 🚧</h2>
            <ul>
              <li class="fragment fade-left">
                A couple of web applications (<strong>portal</strong> and other
                services) send logs to <strong>OpenSearch</strong>
              </li>
              <li class="fragment fade-left">
                The application logs are forwarded to
                <strong>Data Prepper</strong> via
                <strong>fluentbit</strong>
              </li>
              <li class="fragment fade-left">
                <a href="https://opentelemetry.io/">OpenTelemetry</a> traces are
                sent to <strong>OpenSearch</strong>
              </li>
            </ul>
            <aside class="notes" data-markdown>
              <textarea data-template>
              - **Data Prepper**: server-side data collector capable of filtering, enriching, transforming, normalizing,
                and aggregating data for downstream analytics and visualization.
              - Access to <strong>Data Prepper</strong> is whitelisted for those web applications
              - We could use <strong>OpenTelemetry</strong> to send the application logs to <strong>OpenSearch</strong>
              </textarea>
            </aside>
          </section>
          <section>
            <h3>Deployment</h3>
            <div>
              <img src="img/opensearch-docker.png" />
            </div>
            <ul>
              <li class="fragment fade-right">
                <strong>OpenSearch</strong> is deployed via
                <a
                  href="https://git.dcc.sib.swiss/biwg/infrastructure/opensearch"
                  >Ansible</a
                >
              </li>
              <li class="fragment fade-right">Nightly snapshots</li>
              <li class="fragment fade-right">
                Rolling upgrades to a new version are performed manually
              </li>
            </ul>
          </section>
          <section>
            <h3>Dashboards</h3>
            <div>
              <img src="img/opensearch-dashboard.png" width="500" />
            </div>
            <ul>
              <li class="fragment fade-down">
                <strong>OpenSearch Dashboards</strong> is accessible
                <a href="https://fl-10-93.zhdk.cloud.switch.ch/">here</a>
                (access is <em>protected</em>)
              </li>
              <li class="fragment fade-down">
                <strong>Alerting</strong>: custom webhook which sends messages
                to a dedicated <strong>matrix</strong> channel
              </li>
              <li class="fragment fade-down">
                <strong>OpenTelemetry</strong>: latency of the HTTP endpoints
              </li>
            </ul>
            <aside class="notes" data-markdown>
              <textarea data-template>
               - Show **Discover**
               - Show **Observability** > **Logs**/**Traces**
               - Show **OpenSearch Plugins** > **Alerting**
               - HTTP heartbeat monitoring moved to <strong>Prometheus</strong>
              </textarea>
            </aside>
          </section>
          <section>
            <h3>fluentbit/OpenTelemetry Integration</h3>
            <div>
              <img src="img/portal-docker.png" />
            </div>
            <ul>
              <li class="fragment fade-up">
                <strong>fluentbit</strong> configuration could be read on
                <a
                  href="https://gitlab.com/biomedit/portal/-/tree/main/services/fluent-bit"
                  >gitlab.com</a
                >
              </li>
              <li class="fragment fade-up">
                <strong>OpenTelemetry</strong> configuration could be found on
                <a
                  href="https://gitlab.com/biomedit/portal/-/blob/main/services/otel-collector/"
                  >gitlab.com</a
                >
              </li>
            </ul>
          </section>
        </section>
        <section>
          <h3>References</h3>
          <ul>
            <li>
              <a href="https://www.elastic.co/elastic-stack">Elastic Stack</a>
            </li>
            <li>
              <a href="https://opensearch.org/">OpenSearch</a>
            </li>
            <li><a href="https://fluentbit.io/">fluentbit</a></li>
          </ul>
        </section>
        <section>
          <h3>Thanks!</h3>
          <aside class="notes" data-markdown>
            <textarea data-template>
             - **François**
             - **BIWG** team
            </textarea>
          </aside>
        </section>
      </div>
    </div>

    <script src="dist/reveal.js"></script>
    <script src="plugin/notes/notes.js"></script>
    <script src="plugin/markdown/markdown.js"></script>
    <script src="plugin/highlight/highlight.js"></script>
    <script>
      Reveal.initialize({
        hash: true,
        slideNumber: "c/t",
        plugins: [RevealMarkdown, RevealHighlight, RevealNotes],
      });
    </script>
  </body>
</html>
