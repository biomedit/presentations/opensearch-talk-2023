# OpenSearch Ecosystem

Live version: <https://biomedit.gitlab.io/presentations/opensearch-talk-2023>

## Run locally

```bash
git submodule update --init
ln -s reveal.js/{dist,plugin} .
```

Then open `index.html` in your browser.
